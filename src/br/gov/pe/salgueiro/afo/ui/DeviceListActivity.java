package br.gov.pe.salgueiro.afo.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.gov.pe.salgueiro.afo.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import br.gov.pe.salgueiro.afo.model.DeviceNode;

public class DeviceListActivity extends Activity {

	// Retorna Intent extra
	public static String EXTRA_DEVICE_ADDRESS = "device_address";

	// Address preference name
	public static String PREF_DEVICE_ADDRESS = "device_address";
    //Lista de dispositivos bluetooth disponíveis
	private List<DeviceNode> mNodeList = new ArrayList<DeviceNode>();

	private BluetoothAdapter mBtAdapter;
	private DeviceAdapter mDeviceAdapter;
	
	private final OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> av, View v, int location, long id) {
			// Cancel discovery because it's costly and we're about to connect
			mBtAdapter.cancelDiscovery();

			DeviceNode node = (DeviceNode) mDeviceAdapter.getItem(location);
			String address = node.getAddress();

			if (BluetoothAdapter.checkBluetoothAddress(address)) {
				finishActivityWithResult(address);
			}
		}
	};
	
    // The BroadcastReceiver that listens for discovered devices and
    // changes the title when discovery is finished
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                boolean bonded = device.getBondState() == BluetoothDevice.BOND_BONDED;
                int iconId = bonded ? R.drawable.bluetooth_paired : R.drawable.bluetooth;
                // Find is device is already exists
                DeviceNode node = mDeviceAdapter.findByAddress(device.getAddress());
                // Skip if device is already in list                  
                if (node == null) {
                    mDeviceAdapter.add(new DeviceNode(device.getName(), device.getAddress(), iconId));                	
                } else {
                    node.setName(device.getName());
                    node.setIcon(iconId);
                }
                                
            // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                setTitle(R.string.title_select_device);   
                findViewById(R.id.scanLayout).setVisibility(View.VISIBLE);     
            }           
        }
    };
    
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);        
        setContentView(R.layout.device_list);

        // Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            setFinishOnTouchOutside(false);
        }        
            
        // Get the local Bluetooth adapter
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        
        // Initialize array adapters. One for already paired devices and
        // one for newly discovered devices
        mDeviceAdapter = new DeviceAdapter(DeviceListActivity.this,mNodeList);        

        // Initialize the button to perform connect
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final EditText addrView = (EditText) findViewById(R.id.device_address);
        addrView.setText(prefs.getString(PREF_DEVICE_ADDRESS, ""));
        Button connButton = (Button) findViewById(R.id.connect);
        connButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	String address  = addrView.getText().toString();
            	finishActivityWithResult(address);
            }
        });
        
        // Find and set up the ListView for paired devices
        ListView devicesView = (ListView) findViewById(R.id.devices_list);
        devicesView.setAdapter(mDeviceAdapter);
        devicesView.setOnItemClickListener(mDeviceClickListener);

        // Initialize the button to perform device discovery
        Button scanButton = (Button) findViewById(R.id.scan);
        scanButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                doDiscovery();                
            }
        });
        
        // Register for broadcasts when a device is discovered
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);

        // Register for broadcasts when discovery has finished
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, filter);     

        if (mBtAdapter != null && mBtAdapter.isEnabled()) {            
            // Get a set of currently paired devices
            Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
    
            // If there are paired devices, add each one to the ArrayAdapter
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    mDeviceAdapter.add(new DeviceNode(device.getName(), device.getAddress(), R.drawable.bluetooth_paired));
                }
            }           
            findViewById(R.id.title_disabled).setVisibility(View.GONE);
        } else {
            findViewById(R.id.scanLayout).setVisibility(View.GONE);
        }
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor edit = prefs.edit();
        final EditText addrView = (EditText) findViewById(R.id.device_address);
        edit.putString(PREF_DEVICE_ADDRESS, addrView.getText().toString());
        edit.commit();
        
        // Make sure we're not doing discovery anymore
        if (mBtAdapter != null) {
            mBtAdapter.cancelDiscovery();
        }

        // Unregister broadcast listeners
        this.unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onKeyUp( int keyCode, KeyEvent event) {
        if( keyCode == KeyEvent.KEYCODE_BACK) {
            setResult(RESULT_FIRST_USER);
            finish();
            return true;
        }
        return super.onKeyUp( keyCode, event );
    }


    private void doDiscovery() {
        // Indicate scanning in the title
        setProgressBarIndeterminateVisibility(true);
        setTitle(R.string.title_scanning); 
        findViewById(R.id.scanLayout).setVisibility(View.GONE);
       
        // If we're already discovering, stop it
        if (mBtAdapter.isDiscovering()) {
            mBtAdapter.cancelDiscovery();
        }

        // Request discover from BluetoothAdapter
        mBtAdapter.startDiscovery();
    }

    private void finishActivityWithResult(String address) {
		// Create the result Intent and include the MAC address
		Intent intent = new Intent();
		intent.putExtra(EXTRA_DEVICE_ADDRESS, address);

		// Set result and finish this Activity
		setResult(RESULT_OK, intent);
		finish();
	}

}