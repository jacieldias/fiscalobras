package br.gov.pe.salgueiro.afo.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.gov.pe.salgueiro.afo.R;
import br.gov.pe.salgueiro.afo.model.DeviceNode;

public class DeviceAdapter extends ArrayAdapter<DeviceNode> {

	private static int LAYOUT = R.layout.device_node;

	private List<DeviceNode> nodeList = new ArrayList<DeviceNode>();

	public DeviceAdapter(Context context, List<DeviceNode> objects) {
		super(context, LAYOUT, objects);
		this.nodeList = objects;
	}

	public DeviceNode findByAddress(String address) {
		for (DeviceNode d : nodeList) {
			if (address.equals(d.getAddress()))
				return d;
		}
		return null;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		DeviceNode node = getItem(position);

		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(LAYOUT,
					null);

			holder = new ViewHolder();
			holder.img = (ImageView) convertView.findViewById(R.id.icon);
			holder.txtName = (TextView) convertView.findViewById(R.id.name);
			holder.txtAddress = (TextView) convertView
					.findViewById(R.id.address);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.img.setImageResource(R.drawable.bluetooth);
		holder.txtName.setText(node.getName());
		holder.txtAddress.setText(node.getAddress());

		return convertView;
	}

	static class ViewHolder {
		ImageView img;
		TextView txtName;
		TextView txtAddress;
	}
}
