package br.gov.pe.salgueiro.afo.ui;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.UUID;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.gov.pe.salgueiro.afo.R;

import com.datecs.api.printer.Printer;
import com.datecs.api.printer.PrinterInformation;
import com.datecs.api.printer.ProtocolAdapter;

public class MainActivity extends ActionBarActivity {

	// RequestCode para escolha do device
	private static final int REQUEST_GET_DEVICE = 0;
	
	private static final int DEFAULT_NETWORK_PORT = 9100;

	private ProtocolAdapter.ChannelListener mChannelListener = new ProtocolAdapter.ChannelListener() {

		@Override
		public void onReadEncryptedCard() {
			// TODO Auto-generated method stub
		}

		@Override
		public void onReadCard() {
			
		}

		@Override
		public void onReadBarcode() {
		}

		@Override
		public void onPaperReady(boolean state) {
			if (state) {
				toast(getString(R.string.msg_paper_ready));
			} else {
				toast(getString(R.string.msg_no_paper));
			}
		}

		@Override
		public void onOverHeated(boolean state) {
			if (state) {
				toast(getString(R.string.msg_overheated));
			}
		}

		@Override
		public void onLowBattery(boolean state) {
			if (state) {
				toast(getString(R.string.msg_low_battery));
			}
		}
	};

	private Printer mPrinter;
	private ProtocolAdapter mProtocolAdapter;
	private PrinterInformation mPrinterInfo;
	private BluetoothSocket mBluetoothSocket;
	private Socket mPrinterSocket;
	private boolean mRestart;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		findViewById(R.id.btnPrint).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				printText();
			}
		});
		mRestart = true;
		waitForConnection();

	}
	
    @Override
	protected void onDestroy() {
        super.onDestroy();
        mRestart = false;               
        closeActiveConnection();
	}	
		    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GET_DEVICE) {
            if (resultCode == DeviceListActivity.RESULT_OK) {   
            	String address = data.getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
            	//address = "192.168.11.136:9100";
            	if (BluetoothAdapter.checkBluetoothAddress(address)) {
            		establishBluetoothConnection(address);
            	} /*else {
            		establishNetworkConnection(address);
            	}*/
            } else if (resultCode == RESULT_CANCELED) {
                
            } else {
                finish();
            }
        }
    }
    
	private void printText() {
	    doJob(new Runnable() {           
            @Override
            public void run() {
        		StringBuffer sb = new StringBuffer();
        		sb.append("{reset}{center}{w}{h}RECEIPT");
                sb.append("{br}");
                sb.append("{br}");
                sb.append("{reset}1. {b}First item{br}");
                sb.append("{reset}{right}{h}$0.50 A{br}");
                sb.append("{reset}2. {u}Second item{br}");
                sb.append("{reset}{right}{h}$1.00 B{br}");
                sb.append("{reset}3. {i}Third item{br}");
                sb.append("{reset}{right}{h}$1.50 C{br}");
                sb.append("{br}");
                sb.append("{reset}{right}{w}{h}TOTAL: {/w}$3.00  {br}");            
                sb.append("{br}");
                sb.append("{reset}{center}{s}Thank You!{br}");
                
            	try {              
            		mPrinter.reset();            		
                    mPrinter.printTaggedText(sb.toString());                    
                    mPrinter.feedPaper(110); 
                    mPrinter.flush();                                          		
            	} catch (IOException e) {
            		e.printStackTrace();
            	    error(getString(R.string.msg_failed_to_print_text) + ". " + e.getMessage(), mRestart);    		
            	}
            }
	    }, R.string.msg_printing_text);
	}
    
    private void establishBluetoothConnection(final String address) {
    	//closePrinterServer();
        
        doJob(new Runnable() {           
            @Override
            public void run() {      
                BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();                
                BluetoothDevice device = adapter.getRemoteDevice(address);                    
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                InputStream in = null;
                OutputStream out = null;
                
                adapter.cancelDiscovery();
                
                try {
                    mBluetoothSocket = device.createRfcommSocketToServiceRecord(uuid);
                    mBluetoothSocket.connect();
                    in = mBluetoothSocket.getInputStream();
                    out = mBluetoothSocket.getOutputStream();                                        
                } catch (IOException e) {    
                	e.printStackTrace();
                    error(getString(R.string.msg_failed_to_connect) + ". " +  e.getMessage(), mRestart);
                    return;
                }                                  
                
                try {
                    initPrinter(in, out);
                } catch (IOException e) {
                	e.printStackTrace();
                    error(getString(R.string.msg_failed_to_init) + ". " +  e.getMessage(), mRestart);
                    return;
                }
            }
        }, R.string.msg_connecting); 
    }
    
	private void toast(final String text) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (!isFinishing()) {
					Toast.makeText(getApplicationContext(), text,
							Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
    private void dialog(final int iconResId, final String title, final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setIcon(iconResId);
                builder.setTitle(title);
                builder.setMessage(msg);
                
                AlertDialog dlg = builder.create();                
                dlg.show();             
            }           
        });             
    }

    private void error(final String text, boolean resetConnection) {        
        if (resetConnection) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {        
                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();                
                }           
            });
                
            waitForConnection();
        }
    }

    private void doJob(final Runnable job, final int resId) {
        // Start the job from main thread
        runOnUiThread(new Runnable() {            
            @Override
            public void run() {
                // Progress dialog available due job execution
                final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
                dialog.setTitle(getString(R.string.title_please_wait));
                dialog.setMessage(getString(resId));
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                
                Thread t = new Thread(new Runnable() {            
                    @Override
                    public void run() {                
                        try {
                            job.run();
                        } finally {
                            dialog.dismiss();
                        }
                    }
                });
                t.start();   
            }
        });                     
    }

    protected void initPrinter(InputStream inputStream, OutputStream outputStream) throws IOException {
        mProtocolAdapter = new ProtocolAdapter(inputStream, outputStream);
       
        if (mProtocolAdapter.isProtocolEnabled()) {
            final ProtocolAdapter.Channel channel = mProtocolAdapter.getChannel(ProtocolAdapter.CHANNEL_PRINTER);
            channel.setListener(mChannelListener);
            // Create new event pulling thread
            new Thread(new Runnable() {                
                @Override
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        
                        try {
                            channel.pullEvent();
                        } catch (IOException e) {
                        	e.printStackTrace();
                            error(e.getMessage(), mRestart);
                            break;
                        }
                    }
                }
            }).start();
            mPrinter = new Printer(channel.getInputStream(), channel.getOutputStream());
        } else {
            mPrinter = new Printer(mProtocolAdapter.getRawInputStream(), mProtocolAdapter.getRawOutputStream());
        }
        
        mPrinterInfo = mPrinter.getInformation();
        
        runOnUiThread(new Runnable() {          
            @Override
            public void run() {
                ((ImageView)findViewById(R.id.icon)).setImageResource(R.drawable.icon);
                ((TextView)findViewById(R.id.name)).setText(mPrinterInfo.getName());
            }
        });
    }

    private synchronized void closePrinterConnection() {
		if (mPrinter != null) {
			mPrinter.release();
		}

		if (mProtocolAdapter != null) {
			mProtocolAdapter.release();
		}
	}

	private synchronized void closeBlutoothConnection() {
		// Close Bluetooth connection
		BluetoothSocket s = mBluetoothSocket;
		mBluetoothSocket = null;
		if (s != null) {
			try {
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
    public synchronized void waitForConnection() {
        closeActiveConnection();
        
        // Show dialog to select a Bluetooth device. 
        startActivityForResult(new Intent(this, DeviceListActivity.class), REQUEST_GET_DEVICE);
    }

	private synchronized void closeNetworkConnection() {
		// Close network connection
		Socket s = mPrinterSocket;
		mPrinterSocket = null;
		if (s != null) {
			try {
				s.shutdownInput();
				s.shutdownOutput();
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private synchronized void closeActiveConnection() {
		closePrinterConnection();
		closeBlutoothConnection();
		closeNetworkConnection();
	}
}
