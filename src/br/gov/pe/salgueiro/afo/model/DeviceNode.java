package br.gov.pe.salgueiro.afo.model;

public class DeviceNode {
	private String name;
	private String address;
	private int icon;
	
	public DeviceNode(String name, String address, int icon) {
		super();
		this.name = name;
		this.address = address;
		this.icon = icon;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getIcon() {
		return icon;
	}
	public void setIcon(int icon) {
		this.icon = icon;
	}
}
